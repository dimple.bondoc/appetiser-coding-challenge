package com.codingchallenge.myplaylist.module.network

import com.codingchallenge.myplaylist.module.domain.model.SearchResultList

interface NetworkDataSource {
    suspend fun get(): List<SearchResultList>
}