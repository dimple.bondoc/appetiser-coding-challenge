package com.codingchallenge.myplaylist.module.network

import com.codingchallenge.myplaylist.module.network.mappers.SearchResultMapper
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(dagger.hilt.components.SingletonComponent::class)
object NetworkModule {

    @Singleton
    @Provides
    fun provideGsonBuilder(): Gson {
        return GsonBuilder()
            .create()
    }

    @Provides
    fun providesLoggingInterceptor() : HttpLoggingInterceptor {
        return HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    @Provides
    fun providesOkHttpClient(logger: HttpLoggingInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(logger)
            .callTimeout(60, TimeUnit.SECONDS)
            .writeTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .connectTimeout(60, TimeUnit.SECONDS)
            .build()
    }

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson, okHttpClient: OkHttpClient): Retrofit.Builder {
        return Retrofit.Builder()
            .baseUrl("https://itunes.apple.com/")
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(gson))
    }

    @Singleton
    @Provides
    fun providePlayListService(retrofit: Retrofit.Builder): SearchResultApiServices {
        return retrofit
            .build()
            .create(SearchResultApiServices::class.java)
    }

    @Singleton
    @Provides
    fun providePlayListRetrofitService(
        searchResultApiServices: SearchResultApiServices
    ): SearchResultRemoteSource {
        return SearchResultRemoteSourceImpl(
            searchResultApiServices
        )
    }

    @Singleton
    @Provides
    fun provideNetworkDataSource(
        searchResultRemoteSource: SearchResultRemoteSource,
        networkMapper: SearchResultMapper
    ): NetworkDataSource {
        return NetworkDataSourceImpl(
            searchResultRemoteSource,
            networkMapper
        )
    }
}