package com.codingchallenge.myplaylist.module.network.mappers

import com.codingchallenge.myplaylist.module.domain.model.SearchResultList
import com.codingchallenge.myplaylist.module.domain.util.EntityMapper
import com.codingchallenge.myplaylist.module.network.model.SearchResultListEntity
import javax.inject.Inject

class SearchResultMapper
@Inject
constructor(): EntityMapper<SearchResultListEntity, SearchResultList> {
    override fun mapFromEntity(entity: SearchResultListEntity): SearchResultList {
        return SearchResultList(
            wrapperType = entity.wrapperType,
            kind = entity.kind,
            artistId = entity.artistId,
            collectionId = entity.collectionId,
            trackId = entity.trackId,
            artistName = entity.artistName,
            collectionName = entity.collectionName,
            trackName = entity.trackName,
            collectionCensoredName = entity.collectionCensoredName,
            trackCensoredName = entity.trackCensoredName,
            artworkUrl100 = entity.artworkUrl100,
            collectionPrice = entity.collectionPrice,
            trackPrice = entity.trackPrice,
            releaseDate = entity.releaseDate,
            collectionExplicitness = entity.collectionExplicitness,
            trackExplicitness = entity.trackExplicitness,
            discCount = entity.discCount,
            discNumber = entity.discNumber,
            trackCount = entity.trackCount,
            trackNumber = entity.trackNumber,
            trackTimeMillis = entity.trackTimeMillis,
            country = entity.country,
            currency = entity.currency,
            primaryGenreName = entity.primaryGenreName,
            description = entity.description,
            longDescription = entity.longDescription
        )
    }

    override fun mapToEntity(domainModel: SearchResultList): SearchResultListEntity {
        return SearchResultListEntity(
            wrapperType = domainModel.wrapperType.toString(),
            kind = domainModel.kind,
            artistId = domainModel.artistId,
            collectionId = domainModel.collectionId,
            trackId = domainModel.trackId,
            artistName = domainModel.artistName,
            collectionName = domainModel.collectionName,
            trackName = domainModel.trackName,
            collectionCensoredName = domainModel.collectionCensoredName,
            trackCensoredName = domainModel.trackCensoredName,
            artworkUrl100 = domainModel.artworkUrl100,
            collectionPrice = domainModel.collectionPrice,
            trackPrice = domainModel.trackPrice,
            releaseDate = domainModel.releaseDate,
            collectionExplicitness = domainModel.collectionExplicitness,
            trackExplicitness = domainModel.trackExplicitness,
            discCount = domainModel.discCount,
            discNumber = domainModel.discNumber,
            trackCount = domainModel.trackCount,
            trackNumber = domainModel.trackNumber,
            trackTimeMillis = domainModel.trackTimeMillis,
            country = domainModel.country,
            currency = domainModel.currency,
            primaryGenreName = domainModel.primaryGenreName,
            description = domainModel.description,
            longDescription = domainModel.longDescription
        )
    }

    fun mapFromEntityList(entities: List<SearchResultListEntity>): List<SearchResultList>{
        return entities.map { mapFromEntity(it) }
    }
}