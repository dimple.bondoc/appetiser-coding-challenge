package com.codingchallenge.myplaylist.module.network.model

class SearchResultEntity(
    var resultCount : Int,
    var results: List<SearchResultListEntity>
)
