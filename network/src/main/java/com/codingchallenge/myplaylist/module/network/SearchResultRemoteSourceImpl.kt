package com.codingchallenge.myplaylist.module.network

import com.codingchallenge.myplaylist.module.network.model.SearchResultListEntity

internal class SearchResultRemoteSourceImpl
constructor(
    private val searchResultApiServices: SearchResultApiServices
) : SearchResultRemoteSource {

    override suspend fun get(): List<SearchResultListEntity> {
        return searchResultApiServices.getData().results
    }
}