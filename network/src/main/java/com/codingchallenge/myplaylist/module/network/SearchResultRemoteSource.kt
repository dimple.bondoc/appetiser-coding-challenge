package com.codingchallenge.myplaylist.module.network

import com.codingchallenge.myplaylist.module.network.model.SearchResultListEntity

interface SearchResultRemoteSource {
    suspend fun get(): List<SearchResultListEntity>
}