package com.codingchallenge.myplaylist.module.domain.model

class SearchResult(
    var resultCount : Int? = null,
    var results: List<SearchResultList>? = null
)
