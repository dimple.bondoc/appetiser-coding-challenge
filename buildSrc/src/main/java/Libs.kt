object Libs {

    const val kotlinStdlb = "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlinVersion}"

    // Support
    const val appCompat = "androidx.appcompat:appcompat:${Versions.appCompatVersion}"
    const val recyclerView = "androidx.recyclerview:recyclerview:${Versions.recyclerViewVersion}"
    const val supportDesign = "com.google.android.material:material:${Versions.materialDesignVersion}"
    const val fragmentKtx = "androidx.fragment:fragment-ktx:${Versions.fragmentVersion}"

    // Architecture Components
    const val archExtensionsCompiler = "androidx.lifecycle:lifecycle-compiler:${Versions.archCompVersion}"
    const val archRuntimeKtx = "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.archCompVersion}"
    const val liveDataKtx = "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.archCompVersion}"
    const val archLifeCycleJava8 = "androidx.lifecycle:lifecycle-common-java8:${Versions.archCompVersion}"
    const val viewModelKtx = "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.archCompVersion}"
    const val room = "androidx.room:room-runtime:${Versions.roomVersion}"
    const val roomRx = "androidx.room:room-rxjava2:${Versions.roomVersion}"
    const val roomKtx = "androidx.room:room-ktx:${Versions.roomVersion}"
    const val roomCompiler = "androidx.room:room-compiler:${Versions.roomVersion}"

    // Navigation Components
    const val navigationFragments = "androidx.navigation:navigation-fragment:${Versions.navigationVersion}"
    const val navigationFragmentKtx = "androidx.navigation:navigation-fragment-ktx:${Versions.navigationVersion}"
    const val navigationFragmentUI = "androidx.navigation:navigation-ui-ktx:${Versions.navigationVersion}"

    // Core-KTX
    const val coreKTX = "androidx.core:core-ktx:${Versions.coreKtxVersion}"

    // Retrofit
    const val retrofit = "com.squareup.retrofit2:retrofit:${Versions.retrofitVersion}"
    const val retrofitGson = "com.squareup.retrofit2:converter-gson:${Versions.retrofitVersion}"

    // OkHttp
    const val okhttp = "com.squareup.okhttp3:okhttp:${Versions.okhttpVersion}"
    const val okhttpLogging = "com.squareup.okhttp3:logging-interceptor:${Versions.okhttpLoggingVersion}"

    // RxJava
    const val rxjava2 = "io.reactivex.rxjava2:rxjava:${Versions.rxJava2Version}"

    // RxKotlin
    const val rxkotlin = "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlinVersion}"

    // RxAndroid
    const val rxandroid = "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroidVersion}"

    // Dagger Hilt
    const val daggerHiltAndroid = "com.google.dagger:hilt-android:${Versions.daggerHiltVersion}"
    const val daggerHiltAndroidCompiler = "com.google.dagger:hilt-android-compiler:${Versions.daggerHiltVersion}"

    // Dagger Hilt View models
    const val hiltViewModel = "androidx.hilt:hilt-lifecycle-viewmodel:${Versions.daggerHiltExt}"
    const val hiltCompiler = "androidx.hilt:hilt-compiler:${Versions.daggerHiltExt}"

    // Glide
    const val glide = "com.github.bumptech.glide:glide:${Versions.glideVersion}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glideVersion}"

    // Epoxy
    const val epoxy = "com.airbnb.android:epoxy:${Versions.epoxyVersion}"
    const val epoxyProcessor = "com.airbnb.android:epoxy-processor:${Versions.epoxyVersion}"
    const val epoxyDataBinding = "com.airbnb.android:epoxy-databinding:${Versions.epoxyVersion}"

}