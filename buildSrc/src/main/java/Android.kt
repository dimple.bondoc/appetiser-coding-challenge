object Android {

    const val compileSdkVersion = 30
    const val targetSdkVersion = 30
    const val minSdkVersion = 26
    const val buildToolsVersion = "30.0.3"

}