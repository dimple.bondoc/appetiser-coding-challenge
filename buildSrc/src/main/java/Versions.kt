object Versions {
    const val kotlinVersion = "1.4.21"
    const val androidPluginVersion = "4.1.1"
    const val navigationPluginVersion = "2.3.2"

    // Support
    const val appCompatVersion = "1.3.0"
    const val recyclerViewVersion = "1.2.0"
    const val materialDesignVersion = "1.3.0"
    const val fragmentVersion = "1.3.4"

    // Architecture Components
    const val archCompVersion = "2.3.1"
    const val roomVersion = "2.3.0"

    // Navigation Components
    const val navigationVersion = "2.3.5"

    // Core-KTX
    const val coreKtxVersion = "1.5.0"

    // Retrofit
    const val retrofitVersion = "2.9.0"

    // OkHttp
    const val okhttpVersion = "4.9.1"
    const val okhttpLoggingVersion = okhttpVersion

    // RxJava
    const val rxJava2Version = "2.2.21"

    // RxKotlin
    const val rxKotlinVersion = "2.4.0"

    // RxAndroid
    const val rxAndroidVersion = "2.1.1"

    // Hilt
    const val daggerHiltVersion = "2.28.3-alpha"
    const val daggerHiltExt = "1.0.0-alpha03"

    // Glide
    const val glideVersion = "4.12.0"

    // Epoxy
    const val epoxyVersion = "4.3.1"

    // Test dependency versions
    const val androidXJunitVersion = "1.1.2"
    const val espressoVersion = "3.3.0"
    const val junitVersion = "4.13.2"

}