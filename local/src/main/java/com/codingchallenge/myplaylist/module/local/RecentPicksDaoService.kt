package com.codingchallenge.myplaylist.module.local

import com.codingchallenge.myplaylist.module.local.model.RecentlyCacheEntity

interface RecentPicksDaoService {
    suspend fun insert(recentlyCacheEntity: RecentlyCacheEntity)
    suspend fun getAudiobooks(): List<RecentlyCacheEntity>
    suspend fun getTracks(): List<RecentlyCacheEntity>
}