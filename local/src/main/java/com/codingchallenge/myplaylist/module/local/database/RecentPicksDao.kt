package com.codingchallenge.myplaylist.module.local.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.codingchallenge.myplaylist.module.local.model.RecentlyCacheEntity

@Dao
interface RecentPicksDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(searchResultEntity: RecentlyCacheEntity)

    @Query("SELECT * FROM recent_picks WHERE wrapper_type = 'audiobook'")
    suspend fun getAudiobooks(): List<RecentlyCacheEntity>

    @Query("SELECT * FROM recent_picks WHERE wrapper_type = 'track'")
    suspend fun getTracks(): List<RecentlyCacheEntity>

}