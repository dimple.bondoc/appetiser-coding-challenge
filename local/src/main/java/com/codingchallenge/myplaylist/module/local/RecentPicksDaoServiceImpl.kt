package com.codingchallenge.myplaylist.module.local


import com.codingchallenge.myplaylist.module.local.database.RecentPicksDao
import com.codingchallenge.myplaylist.module.local.model.RecentlyCacheEntity

class RecentPicksDaoServiceImpl
constructor(
    private val recentPicksDao: RecentPicksDao
): RecentPicksDaoService {

    override suspend fun insert(recentlyCacheEntity: RecentlyCacheEntity) {
        recentPicksDao.insert(recentlyCacheEntity)
    }

    override suspend fun getAudiobooks(): List<RecentlyCacheEntity> {
        return recentPicksDao.getAudiobooks()
    }

    override suspend fun getTracks(): List<RecentlyCacheEntity> {
        return recentPicksDao.getTracks()
    }
}