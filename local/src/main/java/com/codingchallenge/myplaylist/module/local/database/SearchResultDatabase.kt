package com.codingchallenge.myplaylist.module.local.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.codingchallenge.myplaylist.module.local.model.RecentlyCacheEntity
import com.codingchallenge.myplaylist.module.local.model.SearchResultCacheEntity

@Database(entities = [SearchResultCacheEntity::class, RecentlyCacheEntity::class], version = 1)
abstract class SearchResultDatabase: RoomDatabase() {

    abstract fun searchResultDao(): SearchResultDao
    abstract fun recentPicksDao(): RecentPicksDao

    companion object {
        val DATABASE_NAME: String = "playlist_db"
    }
}