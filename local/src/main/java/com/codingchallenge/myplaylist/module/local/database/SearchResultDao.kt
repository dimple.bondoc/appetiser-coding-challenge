package com.codingchallenge.myplaylist.module.local.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.codingchallenge.myplaylist.module.local.model.SearchResultCacheEntity

@Dao
interface SearchResultDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(searchResultEntity: SearchResultCacheEntity)

    @Query("SELECT * FROM search_result")
    suspend fun get(): List<SearchResultCacheEntity>

}