package com.codingchallenge.myplaylist.interactors

import com.codingchallenge.myplaylist.features.main.DataState
import com.codingchallenge.myplaylist.module.data.cache.CacheDataSourceRecentPicks
import com.codingchallenge.myplaylist.module.domain.model.SearchResultList
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetRecentPickListCache
constructor(
    private val cacheDataSourceRecentPicks: CacheDataSourceRecentPicks
) {

    /**
     * Get recent pick list from cache
     * Show List<SearchResultList>
     */
    suspend fun execute(wrapperType: String): Flow<DataState<List<SearchResultList>>> = flow {
        val recentPicks = if (wrapperType == "track") cacheDataSourceRecentPicks.getTracks() else cacheDataSourceRecentPicks.getAudiobooks()
        emit(DataState.Success(recentPicks))
    }

}