package com.codingchallenge.myplaylist.interactors

import com.codingchallenge.myplaylist.features.main.DataState
import com.codingchallenge.myplaylist.module.data.cache.CacheDataSource
import com.codingchallenge.myplaylist.module.domain.model.SearchResultList
import com.codingchallenge.myplaylist.module.network.NetworkDataSource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetSearchResultList
constructor(
    private val cacheDataSource: CacheDataSource,
    private val networkDataSource: NetworkDataSource
) {

    /**
     * Checks if cache has items
     * Show loading if null
     * Show List<SearchResultList> from cache
     * Get search result from network
     * Insert search result into cache
     * Show List<SearchResultList> from cache
     */

    suspend fun executeGetFromNetwork(): Flow<DataState<List<SearchResultList>>> = flow {
        if (cacheDataSource.get().isNullOrEmpty()) emit(DataState.Loading)
        else emit(DataState.Success(cacheDataSource.get()))
        val searchResult = networkDataSource.get()
        cacheDataSource.insertList(searchResult)
        val cacheSearchResult = cacheDataSource.get()
        emit(DataState.Success(cacheSearchResult))
    }

}