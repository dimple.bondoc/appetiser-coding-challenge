package com.codingchallenge.myplaylist.features.main

import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.airbnb.epoxy.EpoxyAttribute
import com.airbnb.epoxy.EpoxyModelClass
import com.airbnb.epoxy.EpoxyModelWithHolder
import com.codingchallenge.myplaylist.R
import com.codingchallenge.myplaylist.ext.clickWithDebounce
import com.codingchallenge.myplaylist.ext.loadItemImage
import com.codingchallenge.myplaylist.framework.presentation.KotlinEpoxyHolder
import com.codingchallenge.myplaylist.module.domain.model.SearchResultList

@EpoxyModelClass
abstract class SearchResultEpoxyModel: EpoxyModelWithHolder<SearchResultEpoxyModel.Holder>() {

    @EpoxyAttribute lateinit var searchResultList: SearchResultList
    @EpoxyAttribute(EpoxyAttribute.Option.DoNotHash)
    lateinit var onItemSelectListener: (SearchResultList) -> Unit

    override fun getDefaultLayout(): Int = R.layout.search_result_item

    override fun bind(holder: Holder) {
        super.bind(holder)

        holder.run {
            imgArtWork.loadItemImage(searchResultList.artworkUrl100)
            txtTrackName.text = searchResultList.getValidTrackName()
            txtGenre.text = searchResultList.primaryGenreName
            txtPrice.text = searchResultList.getTrackPrice()
        }

        holder.rootView.clickWithDebounce {
            onItemSelectListener.invoke(searchResultList)
        }
    }

    class Holder: KotlinEpoxyHolder() {
        val imgArtWork: ImageView by bind(R.id.imgArtWork)
        val txtTrackName: TextView by bind(R.id.txtTrackName)
        val txtGenre: TextView by bind(R.id.txtGenre)
        val txtPrice: TextView by bind(R.id.txtPrice)
        val rootView: CardView by bind(R.id.viewItem)
    }
}