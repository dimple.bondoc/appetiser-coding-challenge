package com.codingchallenge.myplaylist.features.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.codingchallenge.myplaylist.databinding.FragmentMainBinding

class MainFragment : Fragment() {

    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        binding = FragmentMainBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val viewPagerAdapter = ViewPagerAdapter(childFragmentManager)
        binding.myViewPager.adapter = viewPagerAdapter
        binding.tabLayout.setupWithViewPager(binding.myViewPager)

        /**
         * Every time the user will select an item from the Tracks or Audiobooks
         * The selected item will be saved on the local repo
         * The app will get or load those saved items, during app launch and updates every after the insert method,
         * and displays them on the Recent Picks
         */

    }
}