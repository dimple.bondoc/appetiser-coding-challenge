package com.codingchallenge.myplaylist.framework.presentation

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.codingchallenge.myplaylist.features.main.DataState
import com.codingchallenge.myplaylist.interactors.GetSearchResultList
import com.codingchallenge.myplaylist.interactors.GetRecentPickListCache
import com.codingchallenge.myplaylist.module.data.cache.CacheDataSourceRecentPicks
import com.codingchallenge.myplaylist.module.domain.model.SearchResultList
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class MainViewModel
@ViewModelInject
constructor(
    private val getSearchResultList: GetSearchResultList,
    private val getRecentPickListCache: GetRecentPickListCache,
    private val cacheDataSourceRecentPicks: CacheDataSourceRecentPicks,
    @Assisted private val savedStateHandle: SavedStateHandle
): ViewModel() {

    private val _dataState: MutableLiveData<DataState<List<SearchResultList>>> = MutableLiveData()
    val dataState: LiveData<DataState<List<SearchResultList>>> = _dataState

    private val _dataStateCache: MutableLiveData<DataState<List<SearchResultList>>> = MutableLiveData()
    val dataStateCache: LiveData<DataState<List<SearchResultList>>> = _dataStateCache

    private var itemSelected: SearchResultList? = null

    fun setStateEvent(mainStateEvent: MainStateEvent){
        viewModelScope.launch {
            when(mainStateEvent){
                is MainStateEvent.GetAllSearchResultEvent -> {
                    getSearchResultList.executeGetFromNetwork()
                        .onEach {dataState ->
                            _dataState.value = dataState
                        }
                        .launchIn(viewModelScope)
                }
                is MainStateEvent.SavePlayListEvent -> {
                    cacheDataSourceRecentPicks.insert(itemSelected!!)
                }
                is MainStateEvent.GetAudiobooksCacheEvent -> {
                    getRecentPickListCache.execute("audiobook")
                        .onEach {dataState ->
                            _dataStateCache.value = dataState
                        }
                        .launchIn(viewModelScope)
                }
                is MainStateEvent.GetTracksCacheEvent -> {
                    getRecentPickListCache.execute("track")
                        .onEach {dataState ->
                            _dataStateCache.value = dataState
                        }
                        .launchIn(viewModelScope)
                }
            }
        }
    }

    fun assignSearchResultItem(itemSelected: SearchResultList){
        this.itemSelected = itemSelected
    }
}

sealed class MainStateEvent{
    object GetAllSearchResultEvent: MainStateEvent()
    object GetAudiobooksCacheEvent: MainStateEvent()
    object GetTracksCacheEvent: MainStateEvent()
    object SavePlayListEvent : MainStateEvent()
}

/**
 * Have tried using MVI and Coroutines
 * Just learned it when I was working on this coding challenge
 * Not fully knowledgeable
 */




















