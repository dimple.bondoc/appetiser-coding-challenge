package com.codingchallenge.myplaylist.features.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.codingchallenge.myplaylist.databinding.FragmentSearchResultDetailsBinding
import com.codingchallenge.myplaylist.ext.loadItemImage


class SearchResultDetailFragment : Fragment() {

    private lateinit var binding: FragmentSearchResultDetailsBinding
    private val args: SearchResultDetailFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSearchResultDetailsBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val detail = args.playlist
        with(binding) {
            imgClose.setOnClickListener { activity?.onBackPressed() }
            imgArtWork.loadItemImage(detail.artworkUrl100)
            txtTrackName.text = detail.getValidTrackName()
            txtGenre.text = detail.primaryGenreName
            btnPrice.text = detail.getTrackPrice()
            txtArtistName.text = detail.artistName
            txtAbout.text = "About this ${detail.wrapperType}"
            txtLongDescription.text = detail.getValidDescription()
        }
    }
}