package com.codingchallenge.myplaylist.features.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.codingchallenge.myplaylist.features.audiobook.AudiobookFragment
import com.codingchallenge.myplaylist.features.track.TrackFragment
import kotlinx.coroutines.ExperimentalCoroutinesApi

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val tabTitles = arrayOf("Tracks", "Audiobooks")

    override fun getCount(): Int {
        return 2
    }

    @ExperimentalCoroutinesApi
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                TrackFragment()
            }
            else -> {
                AudiobookFragment()
            }
        }
    }

    override fun getPageTitle(position: Int): CharSequence {
        return tabTitles[position]
    }
}