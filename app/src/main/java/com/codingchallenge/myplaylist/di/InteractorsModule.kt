package com.codingchallenge.myplaylist.di

import com.codingchallenge.myplaylist.interactors.GetSearchResultList
import com.codingchallenge.myplaylist.interactors.GetRecentPickListCache
import com.codingchallenge.myplaylist.module.data.cache.CacheDataSource
import com.codingchallenge.myplaylist.module.data.cache.CacheDataSourceRecentPicks
import com.codingchallenge.myplaylist.module.network.NetworkDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object InteractorsModule {

    @Singleton
    @Provides
    fun provideGetResearchResultList(
        cacheDataSource: CacheDataSource,
        networkDataSource: NetworkDataSource
    ): GetSearchResultList {
        return GetSearchResultList(cacheDataSource, networkDataSource)
    }

    @Singleton
    @Provides
    fun provideGetRecentPickListCache(
        cacheDataSourceRecentPicks: CacheDataSourceRecentPicks
    ): GetRecentPickListCache {
        return GetRecentPickListCache(cacheDataSourceRecentPicks)
    }
}














