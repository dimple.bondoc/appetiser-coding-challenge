package com.codingchallenge.myplaylist.module.data.cache

import com.codingchallenge.myplaylist.module.domain.model.SearchResultList
import com.codingchallenge.myplaylist.module.local.RecentPicksDaoService
import com.codingchallenge.myplaylist.module.local.mappers.RecentPicksCacheMapper

class CacheDataSourceRecentImpl
constructor(
    private val recentPicksDaoService: RecentPicksDaoService,
    private val recentPicksCacheMapper: RecentPicksCacheMapper
): CacheDataSourceRecentPicks {

    override suspend fun insert(searchResultList: SearchResultList) {
        recentPicksDaoService.insert(recentPicksCacheMapper.mapToEntity(searchResultList))
    }

    override suspend fun getAudiobooks(): List<SearchResultList> {
        return recentPicksCacheMapper.mapFromEntityList(recentPicksDaoService.getAudiobooks())
    }

    override suspend fun getTracks(): List<SearchResultList> {
        return recentPicksCacheMapper.mapFromEntityList(recentPicksDaoService.getTracks())
    }
}