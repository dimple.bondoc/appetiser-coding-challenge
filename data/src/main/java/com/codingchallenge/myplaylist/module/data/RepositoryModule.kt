package com.codingchallenge.myplaylist.module.data

import com.codingchallenge.myplaylist.module.data.cache.CacheDataSource
import com.codingchallenge.myplaylist.module.data.cache.CacheDataSourceImpl
import com.codingchallenge.myplaylist.module.data.cache.CacheDataSourceRecentImpl
import com.codingchallenge.myplaylist.module.data.cache.CacheDataSourceRecentPicks
import com.codingchallenge.myplaylist.module.local.RecentPicksDaoService
import com.codingchallenge.myplaylist.module.local.SearchResultDaoService
import com.codingchallenge.myplaylist.module.local.mappers.CacheMapper
import com.codingchallenge.myplaylist.module.local.mappers.RecentPicksCacheMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Singleton
    @Provides
    fun provideCacheDataSource(
        searchResultDaoService: SearchResultDaoService,
        cacheMapper: CacheMapper
    ): CacheDataSource {
        return CacheDataSourceImpl(searchResultDaoService, cacheMapper)
    }

    @Singleton
    @Provides
    fun provideCacheDataRecentSource(
        recentPicksDaoService: RecentPicksDaoService,
        recentPicksCacheMapper: RecentPicksCacheMapper
    ): CacheDataSourceRecentPicks {
        return CacheDataSourceRecentImpl(recentPicksDaoService, recentPicksCacheMapper)
    }

}

























